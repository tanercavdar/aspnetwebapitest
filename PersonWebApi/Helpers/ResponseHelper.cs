﻿using PersonWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonWebApi.Helpers
{
    public class ResponseHelper<T>
    {

        public static GenericResponseModel<T> GetSuccessResponse(T data) {
            return new GenericResponseModel<T>
                {
                    success=true,
                    data = data,
                    message  ="İşlem başarılı."

                };
        }

        public static GenericResponseModel<T> GetErrorResponse(Exception exception)
        {

            return new GenericResponseModel<T>
            {
                success = false,
            message=exception.Message
            };
        }
    }
}