﻿using PersonWebApi.Helpers;
using PersonWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace PersonWebApi.Controllers
{
    //[Authorize] Attribute ile Controller seviyesinde Kimliklendirme ve Yetkilendirme yapılabilir.
    public class PersonController : ApiController
    {

        private static List<PersonModel> _list = new List<PersonModel>();

        static PersonController() {
            _list.Add(new PersonModel { id=1,name="Taner",surname="ÇAVDAR"});
            _list.Add(new PersonModel { id = 2, name = "Oğuzhan", surname = "ÇALIŞKAN" });
            _list.Add( new PersonModel { id=3,name="Şahan",surname="ŞİMŞEK"});
            _list.Add( new PersonModel { id=4,name="Ferhat",surname="KAYA"});
            _list.Add( new PersonModel { id=4,name= "Metin Ali", surname="FEYYAZ"});
        }

        [HttpGet]
        public GenericResponseModel<List<PersonModel>> GetPersons()
        {
            return ResponseHelper<List<PersonModel>>.GetSuccessResponse(_list);
        }

        [HttpGet]
        public GenericResponseModel<PersonModel> GetPersonByID(int id)
        {
                var model = _list.Where(person => person.id == id).FirstOrDefault();
            if (model == null)
            {
                return ResponseHelper<PersonModel>.GetErrorResponse(new Exception("Personel bulunamadı."));
            }
            else {
            return ResponseHelper<PersonModel>.GetSuccessResponse( _list.Where(person => person.id==id).FirstOrDefault());
            }
           
        }
      
       [HttpGet]// Post veya Delete olmalı. Kolayca test edebilmek için HttpGet ile çalışıyor.
        public GenericResponseModel<Boolean> DeletePerson(int id)
        {
            var model = _list.Where(person => person.id == id).FirstOrDefault();
            if (model == null)
            {
                return ResponseHelper<Boolean>.GetErrorResponse(new Exception("Personel bulunamadı."));
            }
            else {
                _list.Remove(model);
                return ResponseHelper<Boolean>.GetSuccessResponse(true);
            }
        }

     
    }
}
