﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonWebApi.Models
{
    public class PersonModel
    {
        public int id { get; set; }
        public String name { get; set; }
        public String surname { get; set; }
    }
}