﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonWebApi.Models
{
    public class GenericResponseModel<T>
    {

        public bool success { get; set; }
        public String message { get; set; }
        public T data { get; set; }

    }
}